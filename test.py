from airflow.decorators import dag, task
from airflow.utils.dates import days_ago
import boto3
from botocore.utils import fix_s3_host
import io
from datetime import datetime
import smb.smb_structs
from smb.SMBConnection import SMBConnection



@dag(default_args={'owner': 'BachakashviliGA'}, schedule_interval=None, start_date=days_ago(0))
def PLEDGES_TO_S3():
    def check_path(client, bucket, file_path):
        result = client.list_objects(Bucket=bucket, Prefix=file_path)
        exists = False
        if 'Contents' in result:
            exists = True
        return exists


    def create_dir(client, bucket, path):
        client.put_object(Bucket=bucket, Key=(path + '/'))
	
	
    userID = r'george' # логин от smb
    password = r'123' # пароль от smb
    client_machine_name = 'george-ubuntu' # название машины, с которой выполняется скрипт
    server_name = r'george-ubuntu' # название машины, на которой лежит smb-share
    server_ip = r'localhost' # ip-адрес машины, на которой лежит smb-share
    domain_name = '' # имя домена (workgroup)
    bucket = 'test-storage1337' # имя бакета
    share = 'sambashare'    # имя расшаренной папки


    @task()
    def pledges_to_s3():

        # ------------------------------------------ SMB

        conn = SMBConnection(userID,
                     password,
                     client_machine_name,
                     server_name,
                     domain_name,
                     use_ntlm_v2=True,
                     sign_options=1
                     )
        conn.connect(server_ip, 445)
        shares = conn.listShares()
        # ------------------------------------------ SMB
        # ------------------------------------------ Boto
        session = boto3.session.Session()
        s3 = session.client(
            service_name='s3',
            endpoint_url='https://storage.yandexcloud.net/'
        )
        resource = boto3.resource(service_name='s3', endpoint_url='https://storage.yandexcloud.net/')
        resource.meta.client.meta.events.unregister('before-sign.s3', fix_s3_host)
        # ------------------------------------------ Boto


        try:
            sharedfiles = conn.listPath(share, '/cft_utlfiles/RCIRU1/ibs/Credit_Registry/PLEDGES/proc/out/')
            if not check_path(s3, bucket, 'opt/pledges'):
                create_dir(s3, bucket, 'opt/pledges')
            for sharedfile in sharedfiles:
                if not sharedfile.isDirectory:
                    print("Found " + sharedfile.filename + " in IBSO folder")
                    f = io.BytesIO()
                    conn.retrieveFile(share, '/cft_utlfiles/RCIRU1/ibs/Credit_Registry/PLEDGES/proc/out/' + sharedfile.filename, f)
                    f.seek(0)
                    obj = resource.Object(bucket, 'opt/pledges/' + sharedfile.filename).upload_fileobj(f)
                    print("File " + sharedfile.filename + " successfully uploaded to bucket " + bucket)
            try:
                tmp = conn.listPath(share, '/cft_utlfiles/RCIRU1/ibs/Credit_Registry/BKI/arch_out/')
            except smb.smb_structs.OperationFailure as err:
                if str(err).__contains__("Unable to open directory") or str(err).__contains__("Path not found"):
                    conn.createDirectory(share, '/cft_utlfiles/RCIRU1/ibs/Credit_Registry/BKI/')
                    conn.createDirectory(share, '/cft_utlfiles/RCIRU1/ibs/Credit_Registry/BKI/arch_out/')
                else:
                    raise err
            now = datetime.now().strftime("%Y.%m.%d_%H-%M-%S")
            try:
                tmp = conn.listPath(share, '/cft_utlfiles/RCIRU1/ibs/Credit_Registry/BKI/arch_out/{}/'.format(now))
            except smb.smb_structs.OperationFailure as err:
                if str(err).__contains__("Unable to open directory") or str(err).__contains__("Path not found"):
                    conn.createDirectory(share, '/cft_utlfiles/RCIRU1/ibs/Credit_Registry/BKI/arch_out/{}/'.format(now))
                else:
                    raise err
            for sharedfile in sharedfiles:
                if not sharedfile.isDirectory:
                    conn.rename(share,
                        '/cft_utlfiles/RCIRU1/ibs/Credit_Registry/PLEDGES/proc/out/' + sharedfile.filename,
                        '/cft_utlfiles/RCIRU1/ibs/Credit_Registry/BKI/arch_out/{}/'.format(now) + sharedfile.filename)

        except smb.smb_structs.OperationFailure as err:
            if str(err).__contains__("Path not found") or str(err).__contains__("Unable to open directory"):
                print("Path '/cft_utlfiles/RCIRU1/ibs/Credit_Registry/PLEDGES/proc/out/' not found")
                raise err
            else:
                raise err
        conn.close()

    pledges_to_s3()

Taskflow = PLEDGES_TO_S3()
